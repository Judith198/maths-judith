<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ConversionsController;

//Soit on renvoie la vue directement dans le routage avec éventuellement les paramètres vides
Route::get('/', function () {
    return view('index');
});

//Soit on passe par le controller et on désigne la méthode à appeler
Route::get('/temperature', [ConversionsController::class, 'getViewTemperature']);

Route::post('/temperature', [ConversionsController::class, 'calcTemperature']);


//Soit on passe par le controller et on désigne la méthode à appeler
Route::get('/distance', [ConversionsController::class, 'getViewDistance']);

Route::post('/distance', [ConversionsController::class, 'calcDistance']);




