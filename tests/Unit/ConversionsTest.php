<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Http\Controllers\Calculs\Conversions;

class ConversionsTest extends TestCase
{
    public function testConvertTempCToF()
    {
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $temperature = 5;
        $uniteInitiale = "°C";
        $unite = "°F";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertTemp($temperature, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 41;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }

    public function testConvertTempFToC()
    {
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $temperature = 5;
        $uniteInitiale = "°F";
        $unite = "°C";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertTemp($temperature, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = -15;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }

    public function testConvertTempFToF()
    {
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $temperature = 5;
        $uniteInitiale = "°F";
        $unite = "°F";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertTemp($temperature, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 5;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }

    public function testConvertKmToKm()
    {
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertDistance
        $distance = 1;
        $uniteInitiale = "km";
        $unite = "km";

        //J'appelle la méthode convertDistance() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 1;

        //Je vérifie que le résultat retourné par convertDistance()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }

    public function testConvertKmToHm()
    {
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertDistance
        $distance = 1;
        $uniteInitiale = "km";
        $unite = "hm";

        //J'appelle la méthode convertDistance() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 10;  // 1km = 10 hm

        //Je vérifie que le résultat retourné par convertDistance()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }

    public function testConvertKmToM()
    {
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertDistance
        $distance = 1;
        $uniteInitiale = "km";
        $unite = "m";

        //J'appelle la méthode convertDistance() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 1000;

        //Je vérifie que le résultat retourné par convertDistance()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }

    public function testConvertKmToDam()
    {
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertDistance
        $distance = 1;
        $uniteInitiale = "km";
        $unite = "dam";

        //J'appelle la méthode convertDistance() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 100;

        //Je vérifie que le résultat retourné par convertDistance()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }

    public function testConvertKmToDm()
    {
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertDistance
        $distance = 1;
        $uniteInitiale = "km";
        $unite = "dm";

        //J'appelle la méthode convertDistance() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 10000;

        //Je vérifie que le résultat retourné par convertDistance()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }

    public function testConvertKmToCm()
    {
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertDistance
        $distance = 1;
        $uniteInitiale = "km";
        $unite = "cm";

        //J'appelle la méthode convertDistance() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 100000;

        //Je vérifie que le résultat retourné par convertDistance()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }

    public function testConvertKmToMm()
    {
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertDistance
        $distance = 1;
        $uniteInitiale = "km";
        $unite = "mm";

        //J'appelle la méthode convertDistance() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 1000000;

        //Je vérifie que le résultat retourné par convertDistance()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
}
