<?php

namespace App\Http\Controllers\Calculs;

class Conversions
{
    public static function convertTemp($temperature, $uniteInitiale, $unite){
        if($uniteInitiale == $unite){
            $res = $temperature;
        }
        else if($uniteInitiale == "°C"){
            $res = $temperature *9/5 + 32;
        }
        else{
            $res = ($temperature - 32) * 5/9;
        }

        return $res;
    }


    public static function convertDistance($distance, $uniteInitiale, $unite){

        if($uniteInitiale == $unite){
            $res = $distance;
        }
        else if($uniteInitiale == "km" && $unite =="hm"){  // 1km = 10 hm
            $res = $distance * 10;
        }
        else if($uniteInitiale == "km" && $unite =="m"){
            $res = $distance * 1000;
        }
        else if($uniteInitiale == "km" && $unite =="dam"){
            $res = $distance * 100;
        }
        else if($uniteInitiale == "km" && $unite =="dm"){
            $res = $distance * 10000;
        }
        else if($uniteInitiale == "km" && $unite =="cm"){
            $res = $distance * 100000;
        }
        else if($uniteInitiale == "km" && $unite =="mm"){
            $res = $distance * 1000000;
        }
        return $res;
    }

}
